/*
 * Copyright 2018 Confluent Inc.
 *
 * Licensed under the Confluent Community License (the "License"); you may not use
 * this file except in compliance with the License.  You may obtain a copy of the
 * License at
 *
 * http://www.confluent.io/confluent-community-license
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OF ANY KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations under the License.
 */

package io.confluent.connect.jdbc.sink;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.errors.ConnectException;
import org.apache.kafka.connect.header.Header;
import org.apache.kafka.connect.header.Headers;
import org.apache.kafka.connect.sink.SinkRecord;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.List;
import java.util.Properties;
import java.util.Collection;
import java.util.HashMap;

import io.confluent.connect.jdbc.dialect.DatabaseDialect;
import io.confluent.connect.jdbc.util.CachedConnectionProvider;
import io.confluent.connect.jdbc.util.TableId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JdbcDbWriter {
  private static final Logger log = LoggerFactory.getLogger(JdbcDbWriter.class);

  private final JdbcSinkConfig config;
  private final DatabaseDialect dbDialect;
  private final DbStructure dbStructure;
  final CachedConnectionProvider cachedConnectionProvider;
  Producer<String, String> producer;
  String reportTopic;

  JdbcDbWriter(final JdbcSinkConfig config, DatabaseDialect dbDialect, DbStructure dbStructure) {
    this.config = config;
    this.dbDialect = dbDialect;
    this.dbStructure = dbStructure;
    List<String> kafkaBrokers = config.kafkaBrokers;
    String kafkaBrokerString = StringUtils.join(kafkaBrokers, ',');
    Properties props = new Properties();
    props.put("bootstrap.servers", kafkaBrokerString);
    props.put("acks", "all");
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
    producer = new KafkaProducer<>(props);
    reportTopic = config.avisoReportTopic;
    this.cachedConnectionProvider = new CachedConnectionProvider(this.dbDialect) {
      @Override
      protected void onConnect(Connection connection) throws SQLException {
        log.info("JdbcDbWriter Connected");
        connection.setAutoCommit(false);
      }
    };
  }

  void write(final Collection<SinkRecord> records) throws SQLException {
    final Connection connection = cachedConnectionProvider.getConnection();

    final Map<TableId, BufferedRecords> bufferByTable = new HashMap<>();
    for (SinkRecord record : records) {
      final TableId tableId = destinationTable(record.topic(), record);
      BufferedRecords buffer = bufferByTable.get(tableId);
      if (buffer == null) {
        buffer = new BufferedRecords(config, tableId, dbDialect, dbStructure, connection);
        bufferByTable.put(tableId, buffer);
      }
      buffer.add(record);
    }
    for (Map.Entry<TableId, BufferedRecords> entry : bufferByTable.entrySet()) {
      TableId tableId = entry.getKey();
      BufferedRecords buffer = entry.getValue();
      List<SinkRecord> recordsInBuffer = buffer.getRecords();
      log.debug("Flushing records in JDBC Writer for table ID: {}", tableId);
      try {
        buffer.flush();
        buffer.close();
        for (SinkRecord record : recordsInBuffer) {
          publishSuccess(record);
        }
      } catch (SQLException se) {
        log.error("Got error. Retrying 1 by 1.");
        connection.rollback();
        connection.setAutoCommit(true);
        List<SinkRecord> failedRecords = buffer.getRecords();
        for (SinkRecord failedRecord : failedRecords) {
          BufferedRecords retryBuffer = new BufferedRecords(config,
                  tableId, dbDialect, dbStructure, connection);
          retryBuffer.add(failedRecord);
          try {
            log.error("Retrying {}", retryBuffer.getRecords().get(0));
            retryBuffer.flush();
            retryBuffer.close();
            publishSuccess(failedRecord);
          } catch (Exception e) {
            //Failed sql write to report
            //Get correlation id off
            log.error("Found an erroring individual record: {} exception message {}",
                    failedRecord, e.getMessage());
            StringBuilder msg = new StringBuilder();
            if (e instanceof SQLException) {
              for (Throwable t : (SQLException) e) {
                msg.append(t.getMessage()).append(" ");
              }
            } else {
              msg.append(e.getMessage());
            }
            publishFailure(failedRecord, msg + " " + ExceptionUtils.getStackTrace(e));
          }
        }
        connection.setAutoCommit(false);
      }

    }
    connection.commit();
  }

  void publishSuccess(SinkRecord record) {
    publishToReportTopic(record, true, "");
  }

  void publishFailure(SinkRecord record, String message) {
    publishToReportTopic(record, false, message);
  }

  void publishToReportTopic(SinkRecord record, Boolean success, String message) {
    Headers recordHeaders = record.headers();
    if (recordHeaders != null) {
      Header correlationId = recordHeaders.lastWithName("correlationId");
      if (correlationId != null) {
        String corrIdString = correlationId.value().toString();
        String jsonMessage = "{\"correlationId\":\""
                + corrIdString + "\", \"success\":\""
                + success.toString() + "\",\"message\":\""
                + message + "\" }";
        try {
          producer.send(
                  new ProducerRecord<String, String>(
                          reportTopic, corrIdString, jsonMessage));
          //log.error("Want to send error here");
        } catch (Exception e) {
          log.error("Could not publish to report topic ", e);
        }
      }
    }

  }

  void closeQuietly() {
    cachedConnectionProvider.close();
  }

  TableId destinationTable(String topic, ConnectRecord record) {
    try {
      Struct valueStruct = (Struct) record.value();
      String tableNameField = (String) valueStruct.get("table_name");
      if (tableNameField != null && !tableNameField.equals("")) {
        return dbDialect.parseTableIdentifier(tableNameField);
      }
    } catch (Exception e) {
      log.info("Could not handle getting table name", e);
    }
    return destinationTable(topic);
  }

  TableId destinationTable(String topic) {
    final String tableName = config.tableNameFormat.replace("${topic}", topic);
    if (tableName.isEmpty()) {
      throw new ConnectException(String.format(
          "Destination table name for topic '%s' is empty using the format string '%s'",
          topic,
          config.tableNameFormat
      ));
    }
    return dbDialect.parseTableIdentifier(tableName);
  }
}
